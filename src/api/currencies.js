import axios from 'axios';

import api from '../config/api';

const getCourse = async (cur1, cur2) => {
    const { data } = await axios.get(`${api.protocol}://${api.host}/price?fsym=${cur1}&tsyms=${cur2}`)

    return data[cur2]
}

const getDailyCourse = async (cur1, cur2) => {
    const course = await axios.get(`${api.protocol}://${api.host}/histoday?fsym=${cur1}&tsym=${cur2}&limit=7&aggregate=1&e=CCCAGG`)

    return course.data.Data
}

export {
    getCourse,
    getDailyCourse
}