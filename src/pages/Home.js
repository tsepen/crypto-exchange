import React from 'react';

import SelectCurrency from '../containers/CurrenciesSelect';
import Chart from '../containers/Chart';

import styles from '../assets/css/style.css';

/**
 * Home page.
 */
export default () => (
    <div className={styles.main}>
        <SelectCurrency />
        <Chart />
    </div>
)