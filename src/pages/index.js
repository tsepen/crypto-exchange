import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router';

import Home from './Home';
import ROUTES from '../constants';
import NotFoundPage from '../components/NotFoundPage';

const Root = ({ store, history }) => (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path={ROUTES.HOME} component={Home} />
        <Route component={NotFoundPage} />
      </Switch>
    </Router>
  </Provider>
);

Root.propTypes = {
  history: PropTypes.instanceOf(Object).isRequired,
  store: PropTypes.instanceOf(Object).isRequired,
};

export default Root;