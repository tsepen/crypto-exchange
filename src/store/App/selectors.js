const getApp = state => state.app;
const getLoading = state => getApp(state).loading;
const getError = state => getApp(state).error;
const getCurrencyList = state => getApp(state).list;
const getDailyCourse = state => getApp(state).dailyCourse;

export default {
    getApp,
    getLoading,
    getError,
    getCurrencyList,
    getDailyCourse
}