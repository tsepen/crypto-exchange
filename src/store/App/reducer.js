import { handleActions } from 'redux-actions';

import produce from 'immer';

import list from '../../data/currencies';

import {
  appInitStarted,
  appInitSuccessed,
  appInitFailed,
  appSetCourse,
  appSetChartDataSuccessed,
  appSetChartDataFailed,
} from './actions';

const initialState = {
  loading: false,
  error: null,
  course: 0,
  dailyCourse: null,
  list
};

export default handleActions({
  [appInitStarted]: state => produce(state, next => {
    next.loading = true;
    next.error = null;
  }),

  [appInitSuccessed]: state => produce(state, next => {
    next.loading = false;
    next.error = null;
  }),

  [appInitFailed]: (state, { payload: { error } }) => produce(state, next => {
    next.loading = false;
    next.error = error;
  }),
  [appSetCourse]: (state, { payload }) => produce(state, next => {
    next.course = payload;
  }),
  [appSetChartDataSuccessed]: (state, { payload }) => produce(state, next => {
    next.dailyCourse = payload;
  }),
  [appSetChartDataFailed]: (state, { payload }) => produce(state, next => {
    next.error = payload;
  }),
}, initialState);
