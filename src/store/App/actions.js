import { createAction } from 'redux-actions';

import { getDailyCourse } from '../../api/currencies';
import { changeCurrencyOneName } from '../CurrencyOne/actions';
import { changeCurrencyTwoName, getActualCourse } from '../CurrencyTwo/actions';

const appInitStarted = createAction('APP/INIT_STARTED');
const appInitSuccessed = createAction('APP/INIT_SUCCESSED');
const appInitFailed = createAction('APP/INIT_FAILED');

const init = () => (
  async dispatch => {
    dispatch(appInitStarted());

    try {
      const cur1 = localStorage.getItem('currencyOne')
      if(cur1) {
        dispatch(changeCurrencyOneName(cur1))
      }
      const cur2 = localStorage.getItem('currencyTwo')
      if(cur2) {
        dispatch(changeCurrencyTwoName(cur2))
      }
      
      dispatch(getActualCourse())
      dispatch(appInitSuccessed());
    } catch (error) {
      dispatch(appInitFailed({
        error: error.message || 'Error',
      }));
    }
  }
)

const appSetChartDataSuccessed = createAction('APP/SET_CHART_DATA_SUCSESSED');
const appSetChartDataFailed = createAction('APP/SET_CHART_DATA_FAILED');

const setChartData = () => (
  async (dispatch, getState) => {
    try {
      const cur1 = getState().currencyOne.name
      const cur2 = getState().currencyTwo.name
      const data = await getDailyCourse(cur1, cur2)

      dispatch(appSetChartDataSuccessed(data));
    } catch (error) {
      dispatch(appSetChartDataFailed({
        error: error.message || 'Error',
      }));
    }
  }
);

const appSetCourse= createAction('APP/SET_COURSE');

const setCourse = course => (
  async dispatch => {
    dispatch(appSetCourse(course));
})

export {
  init,
  setCourse,
  setChartData,

  appInitStarted,
  appInitSuccessed,
  appInitFailed,
  appSetCourse,
  appSetChartDataSuccessed,
  appSetChartDataFailed,
};