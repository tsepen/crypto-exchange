import { createAction } from 'redux-actions';

import validate from '../../helpers/validate';
import { getCourse } from '../../api/currencies';
import { setCourse, setChartData } from '../App/actions';
import { changeCurrencyTwoValue } from '../CurrencyTwo/actions';

const currencyOneValueSuccessed = createAction('CURRENCY/ONE_VALUE_SUCSESSED');
const currencyOneValueFailed = createAction('CURRENCY/ONE_VALUE_FAILED');

const changeCurrencyOneValue = value => (
  async (dispatch, getState) => {
    try {
      if (!validate(value)) { //Валидация ввода пользователя (число)
        dispatch(currencyOneValueFailed({
            error: 'Invalid value'
        }));
      } else {
        const course = getState().app.course; //Получаем текущий курс
        const cur2 = value*course //Считаем количество второй валюты
        //Получаем предыдущее количество второй валюты 
        const currencyTwoValue = getState().currencyTwo.value
         //Если текущее значение второй валюты и предыдущее различаются - обновляем стейт
        if(currencyTwoValue !== cur2) { 
          dispatch(changeCurrencyTwoValue(cur2));
        }
        
        dispatch(currencyOneValueSuccessed(Number(value))); 
      }
    } catch (error) {
      dispatch(currencyOneValueFailed({
        error: error.message || 'Error',
      }));
    }
  }
)

const currencyOneSuccessed = createAction('CURRENCY/ONE_SET_SUCSESSED');
const currencyOneFailed = createAction('CURRENCY/ONE_SET_FAILED');
const currencyOneSetName = createAction('CURRENCY/ONE_SET_NAME_FAILED');

const changeCurrencyOneName = value => (
  async (dispatch, getState) => {

    try {
      dispatch(currencyOneSetName(value));
      localStorage.setItem('currencyOne', value)

      const curTwo = getState().currencyTwo.name;
      if(curTwo && value) {
          const course = await getCourse(value, curTwo)
          dispatch(setCourse(course))
          dispatch(setChartData(value, curTwo))

          const cur2 = getState().currencyTwo.value;
          const cur1 = cur2/course
          dispatch(currencyOneValueSuccessed(cur1));
      }
    } catch (error) {
      dispatch(currencyOneFailed({
        error: error.message || 'Error',
      }));
    }
  }
)

export {
  changeCurrencyOneValue,
  changeCurrencyOneName,

  currencyOneValueSuccessed,
  currencyOneValueFailed,
  currencyOneSuccessed,
  currencyOneFailed,
  currencyOneSetName,
};