import { handleActions } from 'redux-actions';

import produce from 'immer';

import {
  currencyOneValueSuccessed,
  currencyOneValueFailed,
  currencyOneSuccessed,
  currencyOneFailed,
  currencyOneSetName,
} from './actions';

const initialState = {
  value: 0,
  name: 'BTC',
  error: null,
};

export default handleActions({
  [currencyOneValueSuccessed]: (state, { payload }) => produce(state, next => {
    next.value = payload;
    next.error = null;
  }),
  [currencyOneValueFailed]: (state, { payload }) => produce(state, next => {
    next.error = payload.error;
  }),
  [currencyOneSuccessed]: (state, { payload }) => produce(state, next => {
    next.course = payload;
    next.error = null;
  }),
  [currencyOneSetName]: (state, { payload }) => produce(state, next => {
    next.name = payload;
    next.error = null;
  }),
  [currencyOneFailed]: (state, { payload }) => produce(state, next => {
    next.error = payload.error;
  }),
}, initialState);