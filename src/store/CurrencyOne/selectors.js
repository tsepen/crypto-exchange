const getCurrencyOne = state => state.currencyOne;
const getValue = state => getCurrencyOne(state).value;
const getName = state => getCurrencyOne(state).name;
const getError = state => getCurrencyOne(state).error;

export default {
  getCurrencyOne,
  getValue,
  getName,
  getError
}
