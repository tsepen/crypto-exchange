const getCurrencyTwo = state => state.currencyTwo;
const getValue = state => getCurrencyTwo(state).value;
const getName = state => getCurrencyTwo(state).name;
const getError = state => getCurrencyTwo(state).error;

export default {
  getCurrencyTwo,
  getValue,
  getName,
  getError
}