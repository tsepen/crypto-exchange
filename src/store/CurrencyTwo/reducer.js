import { handleActions } from 'redux-actions';

import produce from 'immer';

import {
  currencyTwoValueSuccessed,
  currencyTwoValueFailed,
  currencyTwoSuccessed,
  currencyTwoFailed,
  currencyTwoSetName,
} from './actions';

const initialState = {
  value: 0,
  name: 'ETH',
  error: null,
};

export default handleActions({
  [currencyTwoValueSuccessed]: (state, { payload }) => produce(state, next => {
    next.value = payload;
    next.error = null;
  }),
  [currencyTwoValueFailed]: (state, { payload }) => produce(state, next => {
    next.error = payload.error;
  }),
  [currencyTwoSuccessed]: (state, { payload }) => produce(state, next => {
    next.course = payload;
    next.error = null;
  }),
  [currencyTwoSetName]: (state, { payload }) => produce(state, next => {
    next.name = payload;
    next.error = null;
  }),
  [currencyTwoFailed]: (state, { payload }) => produce(state, next => {
    next.error = payload.error;
  }),
}, initialState);