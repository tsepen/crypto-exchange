import { createAction } from 'redux-actions';

import validate from '../../helpers/validate';
import { getCourse } from '../../api/currencies';
import { setCourse, setChartData } from '../App/actions';
import { changeCurrencyOneValue } from '../CurrencyOne/actions';

const currencyTwoValueSuccessed = createAction('CURRENCY/TWO_VALUE_SUCSESSED');
const currencyTwoValueFailed = createAction('CURRENCY/TWO_VALUE_FAILED');

const changeCurrencyTwoValue = value => (
  async (dispatch, getState) => {
    try {
      if (!validate(value)) {
        dispatch(currencyTwoValueFailed({
            error: 'Invalid value'
        }));
      } else {
        //сохраняем в стейт количество валюты из инпута
          dispatch(currencyTwoValueSuccessed(Number(value)));
        //получаем из стейта курс валют и считаем кол-во валюты 1 по курсу
          const course = getState().app.course;
          const cur1 = value/course
          const currencyOneValue = getState().currencyOne.value

          //если кол-во валюты изменилось - обновляем стейт с новым значением
          if(currencyOneValue !== cur1) {
            dispatch(changeCurrencyOneValue(cur1));
          }
      }
    } catch (error) {
      dispatch(currencyTwoValueFailed({
        error: error.message || 'Error',
      }));
    }
  }
)

const getActualCourse = () => (
    async (dispatch, getState) => {
      //Получаем имена валют
      const curOne = getState().currencyOne.name;
      const curTwo = getState().currencyTwo.name;
      if(curOne && curTwo) {
        //api запрос - получаем текущий курс
        const course = await getCourse(curOne, curTwo)

        //сохраняем курс в redux-стейт
        dispatch(setCourse(course));
        //api запрос - получаем данные для графика и сохраняем в стейт
        dispatch(setChartData(curOne, curTwo))
        
        //считаем количество валюты по курсу и сохраняем в стейт
        const cur1 = getState().currencyOne.value;
        const cur2 = cur1*course
        dispatch(currencyTwoValueSuccessed(cur2));
    }
  }
)

const currencyTwoSuccessed = createAction('CURRENCY/TWO_SET_SUCSESSED');
const currencyTwoFailed = createAction('CURRENCY/TWO_SET_FAILED');
const currencyTwoSetName = createAction('CURRENCY/TWO_SET_NAME_FAILED');

const changeCurrencyTwoName = value => (
  async dispatch => {
    try {
      dispatch(currencyTwoSetName(value)); //диспатчим полученное из селект значение
      localStorage.setItem('currencyTwo', value) //сохраняем в localstorage
      //получем текущий курс
      dispatch(getActualCourse()) 
      //каждые 10 сек обновляем курс
      setInterval(() => dispatch(getActualCourse()), 10000)
    } catch (error) {
      dispatch(currencyTwoFailed({
        error: error.message || 'Error',
      }));
    }
  }
)

export {
  changeCurrencyTwoValue,
  changeCurrencyTwoName,

  currencyTwoValueSuccessed,
  currencyTwoValueFailed,
  currencyTwoSuccessed,
  currencyTwoFailed,
  currencyTwoSetName,
  getActualCourse,
};