
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import app from '../store/App';
import currencyOne from '../store/CurrencyOne';
import currencyTwo from '../store/CurrencyTwo';

const initialState = {};
const loggerMiddleware = createLogger();

// Redux DevTools extension code
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : e => e;

export const history = createHistory();

const configureStore = () => {
  const rootReducer = combineReducers({
    app,
    currencyOne,
    currencyTwo,
    router: routerReducer,
    
  });

  const enhancer = composeEnhancers(applyMiddleware(
    routerMiddleware(history),
    thunkMiddleware,
    loggerMiddleware,
  ));

  return createStore(rootReducer, initialState, enhancer);
};

export default configureStore;