import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Chart from '../components/Chart';
import Preloader from '../components/Preloader';

import appSelectors from '../store/App/selectors';

import styles from './styles.css';

const ChartContainer = ({loading, dailyCourse }) => {
    if (loading) {
        return <Preloader />
    }

    return (
        <section className={styles.chartBlock}>
            <Chart list={dailyCourse} />
        </section>
    )
} 

const mapStateToProps = state => ({
    loading: appSelectors.getLoading(state),
    dailyCourse:  appSelectors.getDailyCourse(state),
})

ChartContainer.propTypes = {
    dailyCourse: PropTypes.array,
    loading: PropTypes.bool.isRequired,
  }

export default connect(mapStateToProps)(ChartContainer);