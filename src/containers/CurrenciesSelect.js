import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from '../components/Button';
import Currency from '../components/Currency';
import Preloader from '../components/Preloader';

import curOneSelectors from '../store/CurrencyOne/selectors';
import curTwoSelectors from '../store/CurrencyTwo/selectors';
import appSelectors from '../store/App/selectors';

import { changeCurrencyOneValue, changeCurrencyOneName } from '../store/CurrencyOne/actions';
import { changeCurrencyTwoValue, changeCurrencyTwoName, getActualCourse } from '../store/CurrencyTwo/actions';
import { setChartData } from '../store/App/actions';

import styles from './styles.css';

const SelectCurrencies = (props) => {
    const {
        loading,
        setValueCurOne,
        setValueCurTwo,
        curOneError,
        curTwoError,
        list,
        selectCurrencyOne,
        selectCurrencyTwo,
        currencyOneValue,
        currencyTwoValue,
        currencyOneName,
        currencyTwoName,
        getCourse,
    } = props;

    if (loading) {
        return <Preloader />
    }
    
    return (
        <section className={styles.exchangeBlock}>
            <Currency
                setValue={setValueCurOne}
                currencyTwo={currencyTwoName}
                error={curOneError}
                list={list}
                selectCurrency={selectCurrencyOne}
                value={currencyOneValue}
                name={currencyOneName}
            />
            <Currency
                setValue={setValueCurTwo}
                currencyTwo={currencyOneName}
                error={curTwoError}
                list={list}
                selectCurrency={selectCurrencyTwo}
                value={currencyTwoValue}
                name={currencyTwoName}
            />
            <Button
                text='Get Actual Rate'
                styleColor='primary'
                getCourse={getCourse}
            />
        </section>
    )
}

const mapStateToProps = state => ({
    loading: appSelectors.getLoading(state),
    curOneError: curOneSelectors.getError(state),
    curTwoError: curTwoSelectors.getError(state),
    list: appSelectors.getCurrencyList(state),
    dailyCourse: appSelectors.getDailyCourse(state),
    currencyOneValue: curOneSelectors.getValue(state),
    currencyTwoValue: curTwoSelectors.getValue(state),
    currencyOneName: curOneSelectors.getName(state),
    currencyTwoName: curTwoSelectors.getName(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setValueCurOne: changeCurrencyOneValue,
    setValueCurTwo: changeCurrencyTwoValue,
    selectCurrencyOne: changeCurrencyOneName,
    selectCurrencyTwo: changeCurrencyTwoName,
    getDailyData: setChartData,
    getCourse: getActualCourse,
}, dispatch);

SelectCurrencies.propTypes = {
    setValueCurOne: PropTypes.func.isRequired,
    setValueCurTwo: PropTypes.func.isRequired,
    getDailyData: PropTypes.func.isRequired,
    selectCurrencyOne: PropTypes.func.isRequired,
    selectCurrencyTwo: PropTypes.func.isRequired,
    curOneError: PropTypes.string,
    curTwoError: PropTypes.string,
    list: PropTypes.array.isRequired,
    getCourse: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectCurrencies);