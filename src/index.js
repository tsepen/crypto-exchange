import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import Root from './pages';

import configureStore, { history } from './store/configureStore';
import { init } from './store/App/actions';

const store = configureStore();

store.dispatch(init());

ReactDOM.render(
    <Root 
        store={store}
        history={history}
    />,
    document.getElementById('root'));

registerServiceWorker();