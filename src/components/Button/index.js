import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

const Btn = ({ text, styleColor, getCourse }) => (
    <Button 
        bsStyle={styleColor}
        onClick={getCourse}
    >
        {text}
    </Button>
)

Btn.propTypes = {
    text: PropTypes.string.isRequired,
    styleColor: PropTypes.string.isRequired
}

Btn.defaultProps = {
    style: ''
}

export default Btn