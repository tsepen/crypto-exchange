import React from 'react';
import PropTypes from 'prop-types';

import { FormControl } from 'react-bootstrap';

import styles from './style.css';

const Currency = props => {
    const { 
        value,
        setValue,
        error,
        list,
        selectCurrency,
        currencyTwo,
        name,
    } = props

    return (
        <article className={styles.form}>
            <div className={styles.currency}>
                <div className={styles.formControl}>
                    <FormControl
                        type="text"
                        value={value}
                        placeholder="Enter value"
                        onChange={e => setValue(e.target.value)}
                    />
                </div>
                <div className={styles.formControl}>
                    <FormControl 
                        componentClass="select"
                        placeholder="select"
                        value={name}
                        onChange={e => selectCurrency(e.target.value)}
                    >
                        {list.map(item => {
                            if (item.shortName === currencyTwo) return false;

                            return (
                                <option 
                                    value={item.shortName}
                                    key={item.shortName}
                                >
                                    {item.name}
                                </option>
                            )
                        })}
                    </FormControl>
                </div>
            </div>
            {error &&  <div className={styles.error}>{error}</div>}
        </article>
    )
}

Currency.propTypes = {
    setValue: PropTypes.func.isRequired,
    selectCurrency: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired,
    list: PropTypes.array.isRequired,
}

export default Currency